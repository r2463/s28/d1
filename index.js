// console.log('Hello World!')

// fetch("URL", {options})


// fetch("http://localhost:3000", {
//     method: "POST",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body:JSON.stringify(
//         //property : value
//     )
// })


// .then(response => response.json())
// .then(response => console.log ())


// fetch('https://jsonplaceholder.typicode.com/posts')
//     .then( data => data.json() )
//     .then( data => {

//         data.forEach(element => {
//             console.log(element)
//         })

//     })


// fetch(`https://jsonplaceholder.typicode.com/posts/1`)
// .then(response => response.json())
// .then(response =>{
//     console.log(response)
// })


// fetch(`https://jsonplaceholder.typicode.com/posts/1`, { 
//     method: "POST",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body: JSON.stringify({
//         title: "New Post",
//         body: "Hello World",
//         userId: 1
//     })
// })

// .then(response => response.json())
// .then(response => {
//     console.log(response)
// })


fetch(`https://jsonplaceholder.typicode.com/posts/1`, { 
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "New Post",
        body: "Hello Updated Post",
        userId: 1
    })
})

.then(response => response.json())
.then(response => {
    console.log(response)
})



fetch(`https://jsonplaceholder.typicode.com/posts/1`, { 
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Corrected Post thru patch method"
   
    })
})

.then(data => data.json())
.then(data => {
    console.log(data)
})


fetch(`https://jsonplaceholder.typicode.com/posts/1`, { 
    method: "DELETE"

})



